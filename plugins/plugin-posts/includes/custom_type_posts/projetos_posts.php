<?php 
    add_action('init','register_project_handler');//em qual momento quer que seja atividade,1° arg = qual momento vai ativar e 2° arg o nome da função
function register_project_handler(){
    $args = array(
        'label' => 'Projetos',
        'description' => 'Projetos para o laboratorio lafeea',
        'public' => true,
        'show_ui' => true,
        'capability_type' => 'post',
        'query_var'=>true, 
        'hierarchical'=> false,
        'supports' => array('custom-fields','title', 'thumbnail'),
        // '"supports"' => array('custom-fields','author','title','editor', 'thumbnail','page-attributes'),
        'publicly_queryable' => true,
        'menu_position'       => 4,
        // 'has_archive'         => true,
        'has_archive'         => 'projetos',
        'exclude_from_search'   => false,
        'rewrite'=>array('slug'=>'projeto','with_front'=> true,'feeds'=>true ),
        // 'rewrite'=>false,
    );
    register_post_type('projetos',$args);//ára registrar produto
    register_taxonomy( 'categories', array('projetos'),array(
        'hierarchical' => true, 
        'label' => 'Categorias', 
        // 'singular_label' => 'Category', 
        // 'rewrite' =>false,
        'rewrite' => array( 'slug' => 'projeto-categoria', 'with_front'=> true,'feeds'=>true ),
        // 'rewrite' => array( 'slug' => '/projetos/' ),
        'show_in_nav_menus'=> true,
        'public' => true,
        'query_var'=>true, 
        'show_in_menu'=> true,
        'query_var'=>true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_quick_edit'=> true,
        'show_admin_column'=>true,
    ),
);
    // register_taxonomy_for_object_type( 'category', 'projetos' );

    // register_taxonomy_for_object_type( 'category', 'projetos' );
    // register_taxonomy( 'projects_cat', 'projetos', array('hierarchical'=>true,'label'=>'Categorias','query_var'=>true,'rewrite'=>true));
}


;?>