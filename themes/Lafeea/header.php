<?php 
/**
 * Header template.
 * 
 * @package Lafeea
 */
?>

<!DOCTYPE html>
<html lang="<?php language_attributes(); ?>">
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(). 'style.css' ?>">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300&family=Poppins&display=swap"
    rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="slick/slick.css" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    
    <!-- <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css"> -->
    <title><?php bloginfo( 'name' ); ?></title>
    <?php wp_head(); ?>
</head>
<body>
  <?php wp_body_open(); ?>
  <header>
    <div class="Header1">
      <a href="/home"><img class="logoHeader" src="<?php echo get_stylesheet_directory_uri() ?>/imgs/logo.png" alt="logo">
      </a>
    </div>
    <nav id="nav_bar">
      <button class="btn_menu" aria-haspop="true"  aria-expanded="false"><span id="hamburguer"></span></button>
      <div class="Header2">
        <a href="/noticias" class="botao">NOTÍCIAS</a>
        <a href="/#sobreNos" class="botao">SOBRE NÓS</a>
        <a href="/projetos?projetos_category=todos" class="botao">PROJETOS</a>
        <a href="/#contato" class="botao">CONTATO</a>
        <a href="/faq" class="botao">FAQ</a>
      </div>
    </nav>
  </header>
