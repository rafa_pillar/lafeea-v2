<?php 
/**
 * Footer template.
 * 
 * @package Lafeea
 */
?>



<footer id="contato">

  <section>
    <a href=""><img class="logo" src="<?php echo get_stylesheet_directory_uri() ?>/imgs/logof.png" alt="logo"></a>

    <div class="contato">

      <div class="tel-email">
        <h3>Entre em contato conosco</h3>
        <div>
          <?php $campos = get_post_meta( 126, 'footer_group', true );
            if ( isset($campos) && !empty($campos)) {?>
      
          <?php foreach ($campos as $campo) { ?>
          <a class="tel">
            <i><img src="<?php echo get_stylesheet_directory_uri() ?>/imgs/icon-phone.png" alt="logo"></img></i>
            <p><?php echo $campo['telefone'];?></p>
          </a>
        
          <a class="email">
            <i><img src="<?php echo get_stylesheet_directory_uri() ?>/imgs/icon-email.png" alt="logo"></img></i>
            <p><?php echo $campo['email']?></p>
          </a>
        <?php }} ?>
      </div>
      </div>
      <div class="email-form">
        <h3> Cadastre-se e receba todas as novidades:</h3>
        <div class="cadastro_form">
        <?php echo do_shortcode('[wpforms id="295"]');?>
          <!-- <input class="cadastro" name="nome" type="text" placeholder="Digite seu e-mail aqui">
          <button class="enviar-btn scale-hover" type="submit">cadastrar</button> -->
        </div>
      </div>
    </div>
    
  </section>
  
  <p class= "copyright">Copyright © 2021, Lafeea. Todos os direitos reservados. Niterói, Brasil.</p>
  <?php wp_footer(); ?>
</footer>

</body>

</html>