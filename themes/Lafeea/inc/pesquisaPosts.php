
<?php

$args = array(
    'post_type' => 'projetos', 
    'posts_per_page' => 3, 
    'tax_query' => array(
        array (
            'taxonomy' => 'categories',
            'field' => 'slug',
            'terms' => 'pesquisa',
        ),
    ), 
);
$the_query = new WP_Query( $args );


if ( $the_query->have_posts() ) {
    echo '<div class="projetos-list-home">';
    while ( $the_query->have_posts() ) {
        $the_query->the_post();
echo '<a class="button  scale-hover" href="'. get_the_permalink() .'"
target="_blank" rel="noopener noreferrer">' . get_the_title() . '</a>';
}
} else {
}
wp_reset_postdata();?>
<a class="button  scale-hover" href="/projetos/?projetos_category=pesquisa"
target="_blank" rel="noopener noreferrer" id="see-more">Ver mais</a>
</div>
