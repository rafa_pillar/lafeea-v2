<?php 
/**
 * Theme Functions.
 * 
 * @package lafeea
 */
// add_action( 'pre_get_posts', 'custom_archive_items' );
// function custom_archive_items( $query ) {
//   if ($query->is_main_query() && !is_admin() && is_post_type_archive( 'projetos')) {
//     $query->set( 'posts_per_page', '1' );
//   }
// }
/* Flush rewrite rules for custom post types. */
add_action( 'after_switch_theme', 'flush_rewrite_rules' );

function default_taxonomy_term( $post_id, $post ) {
    if ( 'publish' === $post->post_status ) {
        $defaults = array(
            'categories' => array('sem categoria'),
        );
        $taxonomies = get_object_taxonomies( $post->post_type );
        foreach ( (array) $taxonomies as $taxonomy ) {
            $terms = wp_get_post_terms( $post_id, $taxonomy );
            if ( empty($terms) && array_key_exists( $taxonomy, $defaults ) ) {
                wp_set_object_terms( $post_id, $defaults[$taxonomy], $taxonomy );
            }
        }
    }
}
add_action( 'save_post', 'default_taxonomy_term', 100, 2 );
//----------------------------------------------------------------TESTE
// function metabox_for_books() {
//     $prefix = '_yourprefix_demo_';

//     $cmb_demo = new_cmb2_box( array(
//         'id'            => $prefix . 'metabox',
//         'title'         => __( 'Test Metabox', 'cmb2' ),
//         'object_types'  => array( 'projetos' ), // Post type
//     ) );
//     $cmb_demo->add_field( array(
//         'name'       => __( 'Book Price', 'cmb2' ),
//         'desc'       => __( 'field description (optional)', 'cmb2' ),
//         'id'         => $prefix . 'text',
//         'type'       => 'text',
//     ) );
// }
// add_action( 'cmb2_admin_init', 'metabox_for_books' );
//################################
//--------------------------------MENU NAVIGATION --------------------------------------------
// function wporg_add_custom_post_types($query) {
//     if ( is_home() && $query->is_main_query() ) {
//         $query->set( 'post_type', array( 'post', 'page', 'movie' ) );
//     }
//     return $query;
// }
// add_action('pre_get_posts', 'wporg_add_custom_post_types');

remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'start_post_rel_link', 10, 0 );
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');

//##################################################################
add_theme_support('post-thumbnails');

 function lafeea_enqueue_scripts(){
	wp_register_style('style-css',get_stylesheet_uri(),[],filemtime(get_template_directory() . '/style.css'),'all');
    wp_register_script( 'main-js', get_template_directory_uri() . '/assets/main.js', [], filemtime( get_template_directory() . '/assets/main.js'), true );
    wp_enqueue_style( 'style-css');
    wp_enqueue_script( 'main-js');
	wp_enqueue_script( 'slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array( 'jquery' ) );
	wp_enqueue_style( 'slick', 'https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.css' );

   }

 add_action( 'wp_enqueue_scripts', 'lafeea_enqueue_scripts' );
	function portfolio_posts_per_page( $query ) {
		
		if(is_post_type_archive('projetos')){
			if ( $query->query_vars['post_type'] == 'projetos' ) $query->query_vars['posts_per_page'] = 9;
			return $query;
		}
	}
		if ( !is_admin() ){add_filter( 'pre_get_posts', 'portfolio_posts_per_page' );}
	
// --------------------PAGE PROJETOS----------------------------------------------------

add_action( 'cmb2_admin_init', 'sobre_nos' );
function sobre_nos() {

    $cmb = new_cmb2_box( array(
        'id'            => 'sobre_nos_metabox',
        'title'         => __( 'Sobre nós:', 'cmb2' ),
        'object_types'  => array( 'page', ), //Post type
        'context'       => 'normal',
        'priority'      => 'high',
		'show_names'    => true, 
		'show_on'      => array( 'key' => 'id', 'value' => array( 126 ) ),
    ) );

//---------------------------------------------SOBRE NÒS--------------------------------------------
	$group_field_id = $cmb->add_field( array(
	    'id'         => 'sobre_nos_group',
		'name'		 => 'Campo "Sobre nós":',
        'type'       => 'group',
        'show_on_cb' => 'cmb2_hide_if_no_cats',
		'repeatable' => false,
		
	
    ) );

	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Imagem:', 'pivot'),
		'desc' => __( ''),
		'id'   => 'sobre_nos_image',
		'type' => 'file',
		'preview_size' => 'medium',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ), 'url' => false ),
	  ) );	

	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Texto sobre nós:', 'pivot'),
		'desc' => __( ''),
		'id'   => 'sobre_nos_text',
		'type' => 'textarea',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ) ),
	) );	

	//---------------------------------------------PESQUISA---------------------------------------------
	$group_field_id = $cmb->add_field( array(
	    'id'         => 'pesquisa_group',
		'name'		 => 'Campo "Pesquisa":',
        'type'       => 'group',
        'show_on_cb' => 'cmb2_hide_if_no_cats',
		'repeatable' => false,
		
	
    ) );

	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Imagem:', 'pivot'),
		'desc' => __( ''),
		'id'   => 'pesquisa_image',
		'type' => 'file',
		'preview_size' => 'medium',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ), 'url' => false ),
	  ) );	

	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Pesquisa:', 'pivot'),
		'desc' => __( ''),
		'id'   => 'pesquisa_text',
		'type' => 'textarea',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ) ),
	) );
	

	//---------------------------------------------ENSINO---------------------------------------------


	$group_field_id = $cmb->add_field( array(
	    'id'         => 'ensino_group',
		'name'		 => 'Campo "Ensino":',
        'type'       => 'group',
        'show_on_cb' => 'cmb2_hide_if_no_cats',
		'repeatable' => false,
		
	
    ) );

	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Imagem:', 'pivot'),
		'desc' => __( ''),
		'id'   => 'ensino_image',
		'type' => 'file',
		'preview_size' => 'medium',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ), 'url' => false ),
	  ) );	

	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Ensino:', 'pivot'),
		'desc' => __( ''),
		'id'   => 'ensino_text',
		'type' => 'textarea',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ) ),
	) );




	//---------------------------------------------EXTENSÃO---------------------------------------------


	$group_field_id = $cmb->add_field( array(
	    'id'         => 'extensao_group',
		'name'		 => 'Campo "Extensão":',
        'type'       => 'group',
        'show_on_cb' => 'cmb2_hide_if_no_cats',
		'repeatable' => false,
		
	
    ) );

	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Imagem:', 'pivot'),
		'desc' => __( ''),
		'id'   => 'extensao_image',
		'type' => 'file',
		'preview_size' => 'medium',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ), 'url' => false ),
	  ) );	

	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Extensão:', 'pivot'),
		'desc' => __( ''),
		'id'   => 'extensao_text',
		'type' => 'textarea',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ) ),
	) );

} 


//--------------------------------Page PROJETOS--------------------------------------------


add_action( 'cmb2_admin_init', 'projeto' );
function projeto() {

    $cmb = new_cmb2_box( array(
        'id'            => 'projeto_metabox',
        'title'         => __( 'Projeto', 'cmb2' ),
        'object_types'  => array( 'projetos'), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
		// 'show_on'      => array( 'key' => 'id', 'value' => array( 33 ) ),
    ) );
    // Regular text field
	
	$group_field_id = $cmb->add_field( array(
        'id'         => 'projeto_group',
        'type'       => 'group',
        'show_on_cb' => 'cmb2_hide_if_no_cats',
		'repeatable' => false,
    ) );
	// $cmb->add_group_field( $group_field_id, array(
	// 	'name' => __( 'Imagem do projeto:', 'pivot'),
	// 	'id'   => 'projeto_image',
	// 	'type' => 'file',
	// 	'preview_size' => 'medium',
	// 	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
	// 	'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ), 'url' => false ),
	//   ) );	
    //   $cmb->add_group_field( $group_field_id, array(
	// 	'name' => __( 'Nome do projeto', 'pivot'),
	// 	'desc' => __( ''),
	// 	'id'   => 'projeto_name',
	// 	'type' => 'text_medium',
	
	// 	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
	// 	// 'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ) )
	// ),);
	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Descrição do projeto', 'pivot'),
		'desc' => __( ''),
		'id'   => 'projeto_text',
		'type' => 'textarea',
		'attributes'  => array(
			'type'=>'number',
			'max'  => '2',
			'required'    => 'required',),
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		// 'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ) )
	) );

	}

add_action( 'cmb2_admin_init', 'participants' );
function participants(){

	$cmb = new_cmb2_box( array(
        'id'            => 'participante_metabox',
        'title'         => __( 'Participantes do projeto', 'cmb2' ),
        'object_types'  => array( 'projetos', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
		// 'show_on'      => array( 'key' => 'id', 'value' => array( 33 ) ),
    ) );

	$membros_group = $cmb->add_field( array(
        'id'         => 'membros_group',
        'type'       => 'group',
        'show_on_cb' => 'cmb2_hide_if_no_cats',
		'repeatable' => true,
		'options'     => array(
			'group_title'   => __( 'Participante {#}', 'mytheme' ), // translatable 
			'add_button'    => __( 'Adicionar participante', 'mytheme' ),
			'remove_button' => __( 'Remover participante', 'mytheme' ),
			// 'sortable'      => true, // beta
		  ),
    ) );

	$cmb->add_group_field( $membros_group, array(
		'name' => __( 'Foto do participante: ', 'pivot'),
		'desc' => __( ''),
		'id'   => 'participante_image',
		'type' => 'file',
		'preview_size' => 'small',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ), 'url' => false ),
	  ) );	

	$cmb->add_group_field( $membros_group, array(
		'name' => __( 'Nome do participante', 'pivot'),
		'desc' => __( ''),
		'id'   => 'participante_name',
		'type' => 'text_medium',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		// 'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ) )
	) );	

	$cmb->add_group_field( $membros_group, array(
		'name' => __( 'Cargo do participante', 'pivot'),
		'desc' => __( ''),
		'id'   => 'participante_role',
		'type' => 'text_medium',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		// 'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ) )
	) );

	$cmb->add_group_field( $membros_group, array(
		'name' => __( 'Link do curriculo do participante', 'pivot'),
		'desc' => __( ''),
		'id'   => 'participante_link_curriculo',
		'type' => 'text_url',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		// 'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ) )
	) );
	}




//Podcast - Page Projetos


add_action( 'cmb2_admin_init', 'podcasts' );
function podcasts() {

    $cmb = new_cmb2_box( array(
        'id'            => 'podcast_metabox',
        'title'         => __( 'Podcasts', 'cmb2' ),
        'object_types'  => array( 'projetos', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
		// 'show_on'      => array( 'key' => 'id', 'value' => array( 33 ) ),
    ) );

    // Regular text field
	$group_field_id = $cmb->add_field( array(
	    'id'         => 'podcast_text',
        'type'       => 'group',
        'show_on_cb' => 'cmb2_hide_if_no_cats',
		'options'     => array(
			'group_title'   => __( 'Podcast {#}', 'mytheme' ), // translatable 
			'add_button'    => __( 'Adicionar podcast', 'mytheme' ),
			'remove_button' => __( 'Remover podcast', 'mytheme' ),
			// 'sortable'      => true, // beta
		  ),
	
    ) );

	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Nome do podcast:', 'pivot'),
		'desc' => __( ''),
		'id'   => 'podcast_name',
		'type' => 'text_small',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ) ),
	  ) );	

	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Link do podcast:', 'pivot'),
		'desc' => __( ''),
		'id'   => 'podcast_link',
		'type' => 'text_url',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ) ),
	) );	
}


//formularios - page projetos
add_action( 'cmb2_admin_init', 'formularios' );
function formularios() {

    $cmb = new_cmb2_box( array(
        'id'            => 'formulario_metabox',
        'title'         => __( 'Formulários', 'cmb2' ),
        'object_types'  => array( 'projetos', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
		// 'show_on'      => array( 'key' => 'id', 'value' => array( 33 ) ),
    ) );

    // Regular text field
	$group_field_id = $cmb->add_field( array(
        'id'         => 'formulario_text',
        'type'       => 'group',
        'show_on_cb' => 'cmb2_hide_if_no_cats',
		'options'     => array(
			'group_title'   => __( 'Formulario {#}', 'mytheme' ), // translatable 
			'add_button'    => __( 'Adicionar formulário', 'mytheme' ),
			'remove_button' => __( 'Remover formulário', 'mytheme' ),
			// 'sortable'      => true, // beta
		  ),
	
    ) );

	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Nome do formulário:', 'pivot'),
		'desc' => __( ''),
		'id'   => 'formulario_name',
		'type' => 'text_small',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ) ),
	  ) );	

	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Link do formulário:', 'pivot'),
		'desc' => __( ''),
		'id'   => 'formulario_link',
		'type' => 'text_url',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ) ),
	) );	
}







// FAQ
add_action( 'cmb2_admin_init', 'faq' );
function faq() {

    $cmb = new_cmb2_box( array(
        'id'            => 'faq_metabox',
        'title'         => __( 'Perguntas frequentes', 'cmb2' ),
        'object_types'  => array( 'page', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
		'show_on'      => array( 'key' => 'id', 'value' => array( 44 ) ),
    ) );

    // Regular text field
	$group_field_id = $cmb->add_field( array(
        // 'name'       => __( 'Perguntas frequentes', 'cmb2' ),
        'id'         => 'faq_group',
        'type'       => 'group',
        'show_on_cb' => 'cmb2_hide_if_no_cats',
		'options'     => array(
			'group_title'   => __( 'Pergunta {#}', 'mytheme' ), // translatable 
			'add_button'    => __( 'Adicionar pergunta', 'mytheme' ),
			'remove_button' => __( 'Remover pergunta', 'mytheme' ),
			// 'sortable'      => true, // beta
		  ),
	
    ) );

	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Pergunta', 'pivot'),
		'desc' => __( ''),
		'id'   => 'pergunta_title',
		'type' => 'text',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ) ),
	  ) );	

	$cmb->add_group_field( $group_field_id, array(
	'name' => __( 'Resposta da pergunta', 'pivot'),
	'desc' => __( ''),
	'id'   => 'pergunta_text',
	'type' => 'textarea',
	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
	'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ) ),
	) );	
}

//--------------------------------------FOOTER---------------------------------------

add_action( 'cmb2_admin_init', 'footer' );
function footer() {

    $cmb = new_cmb2_box( array(
        'id'            => 'footer_metabox',
        'title'         => __( 'Rodapé da página', 'cmb2' ),
        'object_types'  => array( 'page', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		'show_on'      => array( 'key' => 'id', 'value' => array( 126 ) ),
    ) );

    // Regular text field
	$group_field_id = $cmb->add_field( array(
        'id'         => 'footer_group',
        'type'       => 'group',
        'show_on_cb' => 'cmb2_hide_if_no_cats',
		'repeatable' => false, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );

	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Telefone: ', 'pivot'),
		'desc' => __( ''),
		'id'   => 'telefone',
		'type' => 'text_small',
	  ) );	

	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Email: ', 'pivot'),
		'desc' => __( ''),
		'id'   => 'email',
		'type' => 'text_email',
	) );	
}


?>
<?php
/*
Plugin Name: Maria Kelly Carousel
Plugin URI: https://www.damiencarbery.com
Description: Use CMB2 for the content for the [photoshoots_carousel] shortcode.
Author: Damien Carbery
Author URI: https://www.damiencarbery.com/2019/06/use-cmb2-to-create-a-carousel/
Version: 0.1
*/


add_action( 'cmb2_admin_init', 'dcwd_photoshoots_carousel_images' );
function dcwd_photoshoots_carousel_images() {
	$cmb = new_cmb2_box( array(
        'id'            => 'video_url',
        'title'         => __( 'Vídeos:', 'cmb2' ),
        'object_types'  => array( 'projetos', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
		// 'show_on'      => array( 'key' => 'id', 'value' => array( 33 ) ),
    ) );

    // Regular text field
	$group_field_id = $cmb->add_field( array(
        'id'         => 'video_url_group',
        'type'       => 'group',
        'show_on_cb' => 'cmb2_hide_if_no_cats',
		'options'     => array(
			'group_title'   => __( 'Video links {#}', 'mytheme' ), // translatable 
			'add_button'    => __( 'Adicionar vídeo', 'mytheme' ),
			'remove_button' => __( 'Remover vídeo', 'mytheme' ),
			// 'sortable'      => true, // beta
		  ),
	
    ) );

	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Nome do vídeo:', 'pivot'),
		'desc' => __( ''),
		'id'   => 'video_name',
		'type' => 'text_small',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ) ),
	  ) );

	$cmb->add_group_field( $group_field_id, array(
		'name' => __( 'Link do vídeo:', 'pivot'),
		'desc' => __( 'Coloque aqui um link do youtube.'),
		'id'   => 'video_url_text',
		'type' => 'text_url',
		// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
		'options' => array( 'add_row_text' => __( 'Add New Legend', 'mytheme' ) ),
	) );	

}





