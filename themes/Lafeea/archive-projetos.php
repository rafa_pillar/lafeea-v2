<?php
/*
**archive projetos page
*/
get_header();
?>
<?php 
 
;?>

<?php    $count_posts = wp_count_posts( 'projetos' )->publish;?>
<section class="archive_page">
    <h1 class="archive_page_title">Nossos Projetos</h1>
    <?php if($count_posts > 0){;?>
    <div class="archive__files">
        <div class="archive_filter_itens" >
            <ul>
                <a href="/projetos?projetos_category=todos" class="link_archive_projeto todos"><li>Todos</li></a>
                <?php 
                    $terms = get_terms('categories');
                    foreach($terms as $term) {?>
                    <?php  echo '<a class="link_archive_projeto '.$term->slug.'" href="/projetos?projetos_category='.$term->slug.'"><li>'.$term->name. '</li></a>';?>
                    <?php }
                ;?>
            </ul>
        </div>
        <div class="archive_projects_files">
                <?php
            $current = get_query_var('paged') ? get_query_var('paged') : 1;
            $number_of_posts = 9;
            $projetos_category = $_GET['projetos_category'];
            if($projetos_category == 'todos' || empty($projetos_category)){
                $projetos_category = array(
                    'ensino','extensao','pesquisa'
                );
                
            }
            $args = array(
                'post_type' => 'Projetos',
                'post_status'=>'publish',
                'paged'=>$current,
                'tax_query'=>array(
                    array(
                        'taxonomy'=>'categories',
                        'field'=>'slug',
                        'terms'=>$projetos_category,
                    ),
                ),
            );
            $loop = new WP_Query($args);
            
            
            ?>
            <?php 
            while ( $loop->have_posts() ) {
                $loop->the_post();
            ;?>
            
            <a href=  "<?php echo get_the_permalink();?> ">
                <div class="archive_project_single_file">
                    <img class="img_archive_file"src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                    <h2><?php the_title(); ?></h2>
                </div>
            </a>
            <?php
            }
            ;?>
        </div>
        <div class="pagination_box">
        <?php 
           
            if($projetos_category == 'ensino'){
                 $term = get_term( 17, 'categories' );
                $total_in_term = $term->count;
                // echo $total_in_term;
                $count_posts = $total_in_term; 
            }else if($projetos_category == 'extensao'){
                $term = get_term( 19, 'categories' );
                $total_in_term = $term->count;
                // echo $total_in_term;
                $count_posts = $total_in_term; 
            }else if($projetos_category == 'pesquisa'){
                $term = get_term( 18, 'categories' );
                $total_in_term = $term->count;
                // echo $total_in_term;
                $count_posts = $total_in_term; 
             };
            if($count_posts > 9){
                
                echo paginate_links(array(
                    'total'=>ceil($count_posts/$number_of_posts),
                    'current'=>$current,
                    'type'=>'list',
                    'show_all'=>true,
                    'prev_text'=>'<',
                    'next_text' =>'>',
                ));
                
            }
            
        ;?>
          </div>
    </div>
    <?php }
    else{
        echo '
                <div style="display: flex; flex-direction: column; align-items: center;">
                    <p class="coming_soon">Ainda em construção...</p>
                </div>
            ';
    };?>
</section>
<script>
    const params = new URLSearchParams(window.location.search)
    if(params.get('projetos_category')){
        let parameter = document.querySelector('.'+ params.get('projetos_category'));
        console.log(parameter);
        parameter.classList.add('active');
    }
</script>
<?php
get_footer();
?>