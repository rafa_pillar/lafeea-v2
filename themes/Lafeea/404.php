<?php
/**
 * The template for displaying 404 pages (Not Found)
 */

get_header();


?>

<div class="align_center_index">
        <h1>404</h1>
        <h2>UH OH! Você está PERDIDO.</h2>
        <p>A página que você está procurando não existe.
          Como você chegou aqui é um mistério. Mas você pode clicar no botão abaixo
          para voltar à página inicial.
        </p>
        <a href="/home"><button class="btn_green_index"> HOME </button></a>
</div>
<?php
get_footer();
?>