// const bolinha1 = document.getElementById('bolinha1');
// const bolinha2 = document.getElementById('bolinha2');
// const bolinha3 = document.getElementById('bolinha3');
// bolinha1.style.backgroundColor = 'white';
// console.log("teste");
// function bolinhaColor(num) {
//     bolinha1.style.backgroundColor = 'rgba(245, 222, 179, 0.459)';
//     bolinha2.style.backgroundColor = 'rgba(245, 222, 179, 0.459)';
//     bolinha3.style.backgroundColor = 'rgba(245, 222, 179, 0.459)';
//     if (num === 1) {
//         bolinha1.style.backgroundColor = 'white';
//     }
//     else if (num === 2) {
//         bolinha2.style.backgroundColor = 'white';
//     }
//     else {
//         bolinha3.style.backgroundColor = 'white';
//     }
// }


// const carrossel = document.querySelector('.carrossel');
// function scrollPhotos(num) {
//     carrossel.scrollTo(num, 0);
//     console.log(carrossel.scrollLeft)
//     if (num == 0) {
//         bolinhaColor(1);
//     } else if (num == innerWidth) {
//         bolinhaColor(2);
//     }
//     else if (num == innerWidth * 2) {
//         bolinhaColor(3)
//     }
// }


// let num = 0;
//setInterval(()=> {
//    if(num == 0){
//        num = innerWidth;
//    }
//    else if(num == innerWidth){
//        num = innerWidth*2;
//    }
//    else{
//        num = 0;
//    }
//    scrollPhotos(num)
//}, 10000)
// carrossel.addEventListener('scroll', () => {
//     num = carrossel.scrollLeft
//     if (carrossel.scrollLeft == 0) {
//         bolinhaColor(1);
//     } else if (carrossel.scrollLeft == innerWidth) {
//         bolinhaColor(2);
//     }
//     else if (carrossel.scrollLeft == innerWidth * 2) {
//         bolinhaColor(3)
//     }
// })

// bolinha1.addEventListener('click', () => {
//     scrollPhotos(0);
// })
// bolinha2.addEventListener('click', () => {
//     scrollPhotos(innerWidth);
// })
// bolinha3.addEventListener('click', () => {
//     scrollPhotos(innerWidth * 2);
// })

function abre(tagName, btnName) {
    const lista = document.querySelector(tagName)
    const botao = document.querySelector(btnName)
    
    if (!lista.classList.contains("active")){
        botao.innerText = "Ver menos"
        lista.classList.add("active");
    } else {
        botao.innerText = "Ver todos"
        lista.classList.remove("active");
    }
   
}

const btn  = document.querySelectorAll('.titleQuestion')
for (let i = 0; i < btn.length; i++) {
btn[i].addEventListener('click',function(){
    let content = this.nextElementSibling;
    let img = this.lastElementChild 
    img.classList.toggle('rotate')
    if(content.style.maxHeight){
        content.style.maxHeight = null;
    }else{
        content.style.maxHeight = content.scrollHeight + "px";
    }
})
}
//pagina header - menu hamburguer
const btn_mobile = document.querySelector('.btn_menu');
      const toggleMenuHandler = (event) =>{
        if(event.type === 'touchstart'){event.preventDefault()}
        const nav = document.querySelector('#nav_bar');
        nav.classList.toggle('active');
        const active =  nav.classList.contains('active');
        event.currentTarget.setAttribute('aria-expanded',active);
      }
      btn_mobile.addEventListener('click', toggleMenuHandler);
      btn_mobile.addEventListener('touchstart', toggleMenuHandler);
//#####################################################################

jQuery(document).ready(function( $ ) {
	$('#photoshoots_carousel').slick({
	//   centerMode: true,
	  arrows: true,
	  dots: false,
	  autoplay: false,
	//   autoplaySpeed: 3000,
	  slidesToShow: 4,
	  prevArrow: '<button class="slide-arrow prev-arrow"></button>',
	  nextArrow: '<button class="slide-arrow next-arrow"></button>',
	  responsive: [
		{
		  breakpoint: 1200,
		  settings: {
			slidesToShow: 3,
		  }
		},
		{
		  breakpoint: 780,
		  settings: {
			slidesToShow: 2,
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 1,
		  }
		}
	  ]
	});
	
});

//	NOTICIAS

jQuery(document).ready(function( $ ) {
	$('#news_carousel').slick({
	  centerMode: true,
	  arrows: true,
	  dots: false,
	  autoplay: false,
	  focusOnSelect: true,
	  infinite: true,
	//   autoplaySpeed: 3000,
	  slidesToShow: 5,
	  prevArrow: '<button class="slide-arrow-2 prev-arrow-2"></button>',
	  nextArrow: '<button class="slide-arrow-2 next-arrow-2"></button>',
	  responsive: [
		{
		  breakpoint: 1200,
		  settings: {
			slidesToShow: 3,
			arrows:false,
		  }
		},
		{
			breakpoint: 1040,
			settings: {
			  slidesToShow: 3,
			  arrows:false,
			}
		  },
		{
		  breakpoint: 780,
		  settings: {
			slidesToShow: 2,
			arrows: false,
		  }
		},
		{
		  breakpoint: 480,
		  settings: {
			slidesToShow: 1,
			arrows: false,
		  }
		},
		{
		  breakpoint: 320,
		  settings: {
			slidesToShow: 1,
			arrows: false,
			
		  }
		}
	  ]
	});
	
});