<?php get_header(); ?>

<main class="page_exhibition_background">
<?php
    wp_reset_query(); // necessary to reset query
    while ( have_posts() ) : the_post();
    ?>
       
        <section class="page_exhibition_description">
            <div class="page_exhibition_titles"> 
                <h1 class="newExibicion_description_title"><?php the_title(); ?></h1>
                <h2 class="newExibicion_description_subtitle"><?php the_title(); ?></h2>
                <span class="newExibicion_description_data">publicado em <?php the_date(); ?></span>
            </div>
            <div class="newExibicion_description_line"></div>
            <div class="newExibicion_description_imgP">
                <div class="newExibicion_description_img">
                    <?php the_post_thumbnail();?>
                </div>
                <div class="newExibicion_description_P">
                    <p>
                        <?php echo wp_trim_excerpt(); ?>
                    </p>
                    
                </div>
            </div>
        </section>
        <section>
        <?php 
            if (is_page('noticias')){
                $args = array(
                    'category' => 6, //id da categoria noticias
                    'numberposts' => 3);
                    $post = get_posts($args);
                }
                
            $query = new WP_Query( array( 'cat' => 6, 'posts_per_page' => 3 ) );   
        ?>
            <h2 class="lastNewsTitle">Últimas notícias</h2>
            <div class="lastNewsCards">
                <?php if ($query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();?> 
                <a href="<?php the_permalink() ?>">
                    <div class="cardLastNew  scale-hover">
                        <div class="imgLastNewsCard">
                            <?php the_post_thumbnail(); ?>
                        </div>
                        <div class="linkLastNewsCard">
                            <p><?php the_title();?> </p>
                        </div>
                    </div>
                </a>
                <?php endwhile; else : ?>
                <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                <?php endif; ?>
            </div>
        </section>
       <?php endwhile; // End of the loop.
?>
</main>

   

<?php get_footer(); ?>