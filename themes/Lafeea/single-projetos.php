<?php
get_header();
?>
 <main>
<section class="projeto align-content-center">
    <?php
        $campos  = get_post_meta( get_the_ID(), 'projeto_group', true );
        if (!empty($campos)){
        foreach ($campos as $campo) { 
    ?>
    <h1 class="title-font"><?php echo the_title(); ?></h1>
       
    <div class="contour-container">
        <div class="textos">
            <p class="texto-projeto"><?php echo $campo['projeto_text']; ?></p>
        </div>
            
        <div class="img-div">
            <!-- <img src="<?php echo $campo['projeto_image'] ?>" class="img-sala"> -->
            <img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-sala">
        </div>
    </div>
    
    <h1 class="participantes-header">Participantes do projeto:</h1>
    <div class="participantes-div">
        <?php
            $participantes  = get_post_meta( 33, 'membros_group', true );
            if (!empty($participantes)){
            foreach ($participantes as $participante) { 
        ?>
        <div class="participantes scale-hover">
            <div class="info-cara">
                <a class="text-hover-green" href="<?php echo $participante['participante_link_curriculo'] ?>" target="_blank" rel="noopener noreferrer">

                    <div class="img-cara-div">
                        <img src="<?php echo $participante['participante_image']; ?>">
                        
                        <img src="">
                    </div>
                    <div class="cara-texto">
                        <p><?php echo $participante['participante_name']; ?></p>
                        <span>(<?php echo $participante['participante_role']; ?>)</span>
                    </div>
                </a>
            </div>
        </div>
        <?php }} ?>
    </div>
    <?php }} ?>
    
</section>

<section class="videos">
    <h1 class="title-font">videos</h1> 
    <div id="slider-wrap">
        <div id="photoshoots_carousel">

            <?php
            $texts  = get_post_meta( 33, 'video_url_group', true );
            if (!empty($texts)){
            foreach ($texts as $text) {
                ?>
                <div class="div-videos">
                    <div class="div-iframe">    
                        <iframe class="video_carousel_projetos" src='<?php echo $text['video_url_text'];?>' width="250px" height="160px" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen ></iframe>
                    </div>
                    <div class="div-span">

                        <span class="video-name "><?php echo $text['video_name'];?></span>
                    </div>
                </div>
            <?php 
            }
        }
            ?>

        </div>    
    </div>
  
</section>


<section class="podcasts">

    <h1 class="title-font">podcasts</h1>
    
    <div class="listas podcastList">
            <?php
                        

                $texts  = get_post_meta( 33, 'podcast_text', true );
                if (!empty($texts)){
                
                    ?>
            <ul>
            
                <?php foreach ($texts as $text) {?>
                <li><a class="text-hover-green" href='<?php echo $text['podcast_link'];?>' target="_blank" rel="noopener noreferrer"><?php echo $text['podcast_name']; ?></a></li>

                <?php } ?>

                
            </ul>

    </div>
    <div class="esconde">

        <button class="botaoEsconder btnPodcast scale-hover" onclick='abre(".podcastList", ".btnPodcast")'>Ver todos</button>
    </div>
                <?php 
            } else {
                echo "<h2>Não temos podcasts ainda...</h2>";
            }
                ?>
</section>
<section class="podcasts formularios">
    <h1 class="title-font">formularios</h1>
    <!-- <div class="space"></div> -->
    <div class="listas formularioList">
            <?php
            $texts  = get_post_meta( 33, 'formulario_text', true );
            if (!empty($texts)){
            ?>
        <ul>
                <?php foreach ($texts as $text) { ?>
                <li><a class="text-hover-green " href='<?php echo $text['formulario_link'];?>' target="_blank" rel="noopener noreferrer"><?php echo $text['formulario_name']; ?></a></li>
                <?php } ?>
        </ul>
    </div>
    <div class=" esconde esconde-formularios">

        <button class="botaoEsconder btnFormulario scale-hover" onclick='abre(".formularioList", ".btnFormulario")'>Ver
            todos</button>
    </div>
                <?php 
                } else {
                    echo "<h2>Não temos formulários ainda...</h2>";
                }
            ?>
</section>

<?php include get_template_directory().'/inc/formulario.php'; ?>
</main>

<?php 

get_footer(); 
?>