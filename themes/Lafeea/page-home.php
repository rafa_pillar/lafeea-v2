<?php
// Template Name: Home

get_header();

?>

<?php
$args = array(
    'post_type' => 'projetos', 
    'posts_per_page' => 3, 
    'tax_query' => array(
        array (
            'taxonomy' => 'categories',
            'field' => 'slug',
            'terms' => array('ensino','pesquisa','extensao'),
        ),
    ), 
);
$the_query = new WP_Query( $args );
?>
<mai>
<section class="carrossel">
    <?php
    echo do_shortcode('[smartslider3 slider="1"]');
    ?>
</section>

<section class="sobre-nos-home align-content-center" id="sobreNos">
    <div class="space"></div>
    <h1 class="title-font">sobre nós</h1>
    
    <div class="space"></div>
    <div class="contour-container" >
        
        <?php $campos = get_post_meta( 126, 'sobre_nos_group', true ); 
        // print_r($campos);
        if ( isset($campos) && !empty($campos)) {
         foreach ($campos as $campo) { ?>
        <div class="textos">
            <p>
                <?php  echo $campo['sobre_nos_text']; ?>
            </p>
        </div>
        <div class="img-div">
            <img src="<?php echo $campo['sobre_nos_image']; ?>" />
        </div>
        <?php 
        }
        } else {
            echo '<p>Ainda em construção...</p>';
        }
        ?>
    </div>
</section>
    <div class="space"></div>
    
    <section class="nossos-projetos-home">
        <div class="space"></div>
        <h1 class="title-font">nossos projetos</h1>
        <div class="space"></div>
        <div class="contour-container">
        <?php $campos = get_post_meta( 126, 'pesquisa_group', true );
        if ( isset($campos) && !empty($campos)) {
            foreach ($campos as $campo) { ?>
        <div class="img-div">
            <img src="<?php echo $campo['pesquisa_image']?>" />
        </div>
        <div class="textos">
            <h2 class="title-font">Pesquisa</h2>
            <p><?php  echo $campo['pesquisa_text'] ?></p>
            <?php } ?>
             
            <?php include get_template_directory().'/inc/pesquisaPosts.php'; ?>
            <?php 
            } else {
                echo '<div style="display: flex; flex-direction: column; align-items: center;">
                <h2 class="title-font">Pesquisa</h2>
                    <p>Ainda em construção...</p>
                    </div>';
            }
            ?>
            </div>
        </div>
    </div>
    <div class="space"></div>
    <div class="contour-container">
        <?php $campos = get_post_meta( 126, 'ensino_group', true ); 
        if ( isset($campos) && !empty($campos)) {
            foreach ($campos as $campo) { ?>
            <div class="textos">
                <h2 class="title-font">Ensino</h2>
                <p><?php  echo $campo['ensino_text'] ?></p>
                <div class="projetos-list-home">
                <?php } ?>
                <?php include get_template_directory().'/inc/ensinoPosts.php'; ?>
                </div>
            </div>
        <?php foreach ($campos as $campo) { ?>
                <div class="img-div">
                    <img src="<?php echo $campo['ensino_image']?>" />
                </div>
            </div>
            <?php 
            }
            } else {
                echo '<div style="display: flex; flex-direction: column; align-items: center;">
                <h2 class="title-font">Ensino</h2>
                    <p>Ainda em construção...</p>
                    </div>';
            }
            ?>
    <sdiv class="space"></sdiv>
    <div class="contour-container">
        
        <?php $campos = get_post_meta( 126, 'extensao_group', true ); 
        if ( isset($campos) && !empty($campos)) {
            foreach ($campos as $campo) { ?>
            <div class="img-div">
                <img src="<?php echo $campo['extensao_image']?>" />
            </div>
            <div class="textos">
                <h2 class="title-font">Extensão</h2>
                <p>
                    <?php echo $campo['extensao_text']; ?>
                </p>
                <div class="projetos-list-home">
        <?php } ?>
                
        <?php include get_template_directory().'/inc/extensaoPosts.php'; ?>
        <?php 
                
            } else {
                echo '<div style="display: flex; flex-direction: column; align-items: center;">
                    <h2 class="title-font">Extensão</h2>
                    <p>Ainda em construção...</p>
                    </div>';
            }
            ?>
            </div>
        </div>
    </div>
    <div class="space"></div>
</section>



<?php include get_template_directory().'/inc/formulario.php'; ?>
</main>
<?php
get_footer();
?>