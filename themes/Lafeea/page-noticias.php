<?php
/*
Template Name: Noticias
*/
get_header();
?>
<?php 
if (is_page('noticias')){
    $args = array(
        'category' => 6, //id da categoria noticias
        'numberposts' => 3);
        $post = get_posts($args);
    }
 	
$query = new WP_Query( array( 'cat' => 6, 'posts_per_page' => 2 ) );   
?>
<main class="main-noticias">

    <h1 class="MainNews_page_title">Principais notícias</h1>
    <section class="allMyNews">
        <div class="postsNews">
            <?php if ($query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();?> 
            <a href="<?php the_permalink() ?>">
            <div class="post01News">
                <div class="imgPostNews">
                    <div class="img_post_mainNews">
                        <?php the_post_thumbnail() ?>
                    </div>
                </div>
                <div class="descriptionPostNews">
                    <p class="linkNews  scale-hover "style="transition: 0.5s;" >
                    <?php the_title(); ?>
                    </p>
                    <span class="data_create">Postado em: <?php the_date(); ?></span>
                </div>
            </div>
        </a>
        <div class="lineMainNews"></div>
        <?php endwhile; else : ?>
            <p><?php esc_html_e( 'Não temos notícias ainda...' ); ?></p>
        <?php endif; ?>
    </div>

    <?php echo do_shortcode('[yarpp template="yarpp-template-thumbnail"]');?>
</section>
<section class="carouselVideoNews">

    <h2 class="lastNewsTitle">Mais notícias</h2>
   

    <div id="slider-wrap">
        <div id="news_carousel">
   
            
            <?php 
                $query = new WP_Query( array( 'cat' => 6, 'posts_per_page' => 5 ) );   
                $count_posts = wp_count_posts();
                
                if ($query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();?> 
                <div class="cardLastNewCarousel scale-hover" style="transition: 0.5s;">
                    <a href="<?php the_permalink() ?>">
                        <div class="imgLastNewsCardCarousel">
                            <?php the_post_thumbnail() ?>
                        </div>
                        <div class="linkLastNewsCardCarousel">
                            <p><?php the_title();?> </p>
                        </div>
                    </a>
                </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
    
</section>

</main>

<?php 
wp_mail("danielvlima98@gmail.com.com", "test", "test");
get_footer(); 
?>