<?php
// Template Name: faq
get_header(); ?>

<main>
<h1 class="faq_page_title">Faq - perguntas frequentes</h1>
<div class="espacamento">
    <div class="allQuestions">
        <?php while ( have_posts() ) : the_post(); ?>

            <?php get_template_part( 'content', 'page' ); ?>

            <?php $texts  = get_post_meta( get_the_ID(), 'faq_group', true );
                if (!empty($texts)){
                foreach ($texts as $text) {
            ?>
            <div class="individualsQuestions">
                <div class="titleQuestion">
                    <h2><?php echo $text['pergunta_title'];?></h2>
                    <div class="imgQuestion">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/arrowDown.png" alt="">
                    </div>
                </div>
                <div class="descriptionQuestion">
                    <p><?php echo $text['pergunta_text'];?> </p>
                </div>
            </div>
                <?php 
                }
            }
                ?>
            <?php endwhile; ?>
    </div>
</div>
<?php get_footer(); ?>
</main>